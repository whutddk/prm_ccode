/** ---------------------------------------------------------------------------
 * Copyright (c) 2018, Wuhan University of Technology All rights reserved.
 *
 * @file    prm_api.c
 * @date    2018-11-16
 * @version 0.1.1  2018.11
---------------------------------------------------------------------------- */


#include <stdint.h>
#include "math/fast_math.h"
#include "PRM_api.h"

#include "stdlib.h"
#include <string.h>

#include "math.h"


#include "library/common/libxil.h"
#include "math/tools.h"

struct PRM_GLOBAL prmGlobal;




/**
 * [head_pose_update description] connect start pose to all reachable pose 
 * 									every time when obstacle move
 * @return [description]		-2 direct to endpose
 *                        		-1 mollow fail
 *                        		 0 finish
 */
static int8_t head_pose_update()
{
	// uint32_t minCost = 0xffffffff;
	// uint16_t minIndex = 0;
	uint32_t edgeCost;

	uint32_t startCost[5] = {0xffffffff,0xffffffff,0xffffffff,0xffffffff,0xffffffff};
	uint16_t startIndex[5] = {0,0,0,0,0};

	for ( uint16_t index = 0; index < POSE_NUM ;index ++ )
	{
		edgeCost = graph_cost_cal( poseTable + index,poseTable + 98 );

		for ( uint8_t i = 0; i < 5; i++ )
		{
			if ( edgeCost < startCost[i] )
			{
				startCost[i] = edgeCost;
				startIndex[i] = index;
				break;
			}
			else
			{
				;
			}			
		}				
	}

	for ( uint8_t i = 0; i < 5; i ++ )
	{
		if (0 == graph_add_activePose(startIndex[i],98,startCost[i]) )
		{
			;
		}
		else 
		{
			return -1;
		}
	}



	// for ( uint16_t i = 0; i < POSE_NUM ;i ++ )
	// {
	// 	edgeCost = graph_cost_cal( poseTable + i,poseTable + 98 );

	// 	if ( edgeCost < minCost )
	// 	{
	// 		minCost = edgeCost;
	// 		minIndex = i;
	// 	}

	// }			
	// /* add to actice gather */
	// if (0 == graph_add_activePose(minIndex,98,edgeCost))
	// {
	// 	;
	// }
	// else
	// {
	// 	return -1;
	// }
	return 0;

}

/**
 * [tail_pose_update description] connect end Pose to all pose expect start pose
 * 									every time when obstacle move
 */
static int32_t tail_pose_update()
{

	uint32_t edgeCost;

	uint32_t finalCost[5] = {0xffffffff,0xffffffff,0xffffffff,0xffffffff,0xffffffff};
	uint16_t finalIndex[5] = {0,0,0,0,0};

	for ( uint16_t index = 0; index < POSE_NUM ;index ++ )
	{
		edgeCost = graph_cost_cal( poseTable + index,poseTable + 99 );

		for ( uint8_t i = 0; i < 5; i++ )
		{
			if ( edgeCost < finalCost[i] )
			{
				finalCost[i] = edgeCost;
				finalIndex[i] = index;
				break;
			}
			else
			{
				;
			}			
		}				
	}

	for ( uint8_t i = 0; i < 5; i ++ )
	{
		if ( 0 == graph_add_final(finalIndex[i]) )
		{
			continue;
		}
		else 
		{
			return -1;
		}
	}


	return 0;
}


/**
 * input a serial of ovstacle grid and get Mask of 1034 edge 
 */
static int32_t prm_collision_check()
{
	int8_t status;
	status = head_pose_update();
	if ( 0 == status )
	{
		;
	}
	else 
	{
		return -1;
	}

	status = collision_chk_all_ob();
	if ( -1 == status )
	{
		return -1;
	}
	else
	{
		;
	}
	
	status = tail_pose_update();
	if ( 0 == status )
	{
		;
	}
	else 
	{
		return -1;
	}

	return 0;
}



/**
 * [prm_init description] 		clear 3 pointer
 * 								set start pose and set end pose
 */	
void prm_init()
{
	prmGlobal.obIndexHeader = NULL;
	prmGlobal.reachPoseHeader = NULL;
	prmGlobal.reachPoseCnt = 0;
	prmGlobal.finalPoseHeader = NULL;
	prmGlobal.routeMap = NULL;
	prmGlobal.saftyEdgeHeader = NULL;
/********** Config the start pose ********************/

	prmGlobal.startConfig.shoulderPos = 1;
	prmGlobal.startConfig.armPos = 0;
	prmGlobal.startConfig.elbowPos = 0;
	prmGlobal.startConfig.wristPos = 0;
	prmGlobal.startConfig.fingerPos = 0;
	prmGlobal.startConfig.toolPos = 0;

	poseTable[98].shoulderPos = prmGlobal.startConfig.shoulderPos;
	poseTable[98].armPos = prmGlobal.startConfig.armPos;
	poseTable[98].elbowPos = prmGlobal.startConfig.elbowPos;
	poseTable[98].wristPos = prmGlobal.startConfig.wristPos;
	poseTable[98].fingerPos = prmGlobal.startConfig.fingerPos;
	poseTable[98].toolPos = prmGlobal.startConfig.toolPos;


/************** config the end pose ***********************************/

	prmGlobal.endConfig.shoulderPos = -180 * 64;
	prmGlobal.endConfig.armPos = 0;
	prmGlobal.endConfig.elbowPos = 0;
	prmGlobal.endConfig.wristPos = 0;
	prmGlobal.endConfig.fingerPos = 0;
	prmGlobal.endConfig.toolPos = 0;

	poseTable[99].shoulderPos = prmGlobal.endConfig.shoulderPos;
	poseTable[99].armPos = prmGlobal.endConfig.armPos;
	poseTable[99].elbowPos = prmGlobal.endConfig.elbowPos;
	poseTable[99].wristPos = prmGlobal.endConfig.wristPos;
	poseTable[99].fingerPos = prmGlobal.endConfig.fingerPos;
	poseTable[99].toolPos = prmGlobal.endConfig.toolPos;

}


static int32_t prm_update_obstacle()
{
	if (-1 == obstacle_get())
	{
		return -1;
	}
	return 0;
}



int32_t prm_process()
{
	int32_t status;

	status = prm_update_obstacle();

	if ( -1 == status )
	{
		return -1;
	}


	status = prm_collision_check();
	if (-2 == status)
	{
		return -2;
	}
	else if ( -1 == status )
	{
		return -1;
	}

	status = graph_flatten();
	if ( -1 == status )
	{
		return -1;
	}


	status = graph_route_map();
	if ( -1 == status )
	{
		return -1;
	}

	return 0;



}





