/** ---------------------------------------------------------------------------
 * Copyright (c) 2018, Wuhan University of Technology All rights reserved.
 *
 * @file    prm_config.h
 * @date    2018-11-16
 * @version 0.1.1  2018.11
---------------------------------------------------------------------------- */

#ifndef _PRM_CONFIG_H_
#define _PRM_CONFIG_H_

#include "stdlib.h"

#define POSE_NUM	65
#define EDGE_NUM	1034


#define PRM_ANNO_ARM_PRECISION 5
#define PRM_ANNO_WRIST_PRECISION  10
#define PRM_ANNO_TOOL_PRECISION  15

#endif





