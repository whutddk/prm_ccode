/** ---------------------------------------------------------------------------
 * Copyright (c) 2018, Wuhan University of Technology All rights reserved.
 *
 * @file    prm.h
 * @date    2018-11-16
 * @version 0.1.1  2018.11
---------------------------------------------------------------------------- */

#ifndef _PRM_H_
#define _PRM_H_

#include <stdlib.h>

#include "../system/assert.h"
#include "math/qformat.h"
#include "model/anno.h"

// #define DEBUG_PRINT
// #define DEBUG_ASSERT


/** for graph search **/
struct REACHABLE_POSE{
	uint16_t poseIndex;
	uint16_t parentsIndex;
	uint32_t cost;				//distance form start to this pose through other pose
	// uint32_t distance2End;
	struct REACHABLE_POSE *next;
};

/** for graph search **/
struct FINAL_POSE{
	uint16_t poseIndex;
	uint32_t distance2End;	//record q6_t format
	struct FINAL_POSE *next;
};


/** for sensor record */
struct OB_INDEX_NODE{
	uint16_t obIndex;				//0~16384
	struct OB_INDEX_NODE *next;

};

struct EDGE_SAFTY_INDEX{
	uint16_t edgeIndex;
	struct EDGE_SAFTY_INDEX *next;
};

struct ROUTE_MAP{
	uint16_t poseIndex;
	struct ROUTE_MAP* next; 
};



struct PRM_GLOBAL{

	SixD_Configure startConfig;
	SixD_Configure endConfig;

	struct OB_INDEX_NODE *obIndexHeader;

	struct REACHABLE_POSE *reachPoseHeader;
	uint32_t reachPoseCnt;

	struct FINAL_POSE *finalPoseHeader;

	struct EDGE_SAFTY_INDEX *saftyEdgeHeader;

	struct ROUTE_MAP *routeMap;

	uint16_t finalIndex;

	uint32_t edgeMask[32];

};






#endif





