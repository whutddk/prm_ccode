/** ---------------------------------------------------------------------------
 * Copyright (c) 2018, Wuhan University of Technology All rights reserved.
 *
 * @file    obstacle.c
 * @date    2018-11-16
 * @version 0.1.1  2018.11
---------------------------------------------------------------------------- */

/**
 *  get a serial of obstacle from any kind of sensor
 */


#include "math/fast_math.h"

#include "prm/PRM_api.h"

#include <stdint.h>
#include "string.h"



/**
 * create a serial of obstacle by sensor or user function
 */
int32_t obstacle_get()
{
	uint16_t X,Y,Z;

	struct OB_INDEX_NODE *obNode = NULL;



	for ( X = 0;X < 16;X++ )
		for ( Y = 0;Y < 32 ;Y ++ )
			for ( Z = 20;Z < 32;Z++ )
			{
				obNode = (struct OB_INDEX_NODE *)malloc(sizeof(struct OB_INDEX_NODE));

				if ( (NULL == obNode))
				{
					return -1;
				}


				obNode->obIndex = X*1024 + Y*32 + Z;

				obNode->next = prmGlobal.obIndexHeader;
				prmGlobal.obIndexHeader = obNode;
			}


	return 0;
}




