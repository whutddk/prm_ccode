/** ---------------------------------------------------------------------------
 * Copyright (c) 2018, Wuhan University of Technology All rights reserved.
 *
 * @file    graph.cpp
 * @date    2018-11-16
 * @version 0.1.1  2018.11
---------------------------------------------------------------------------- */

#include "math/fast_math.h"
#include "prm/prm_api.h"


/**
 * [graph_cost_cal description]
 * @param  pose1 [description]
 * @param  pose2 [description]
 * @return       [description]
 */
uint32_t graph_cost_cal( struct SixD_Configure* pose1,struct SixD_Configure* pose2 )
{
	uint32_t edgeCost;

	edgeCost = FAST_ABS(pose1->shoulderPos - pose2->shoulderPos)
				+ FAST_ABS(pose1->armPos - pose2->armPos)
				+ FAST_ABS(pose1->elbowPos - pose2->elbowPos)
				+ FAST_ABS(pose1->wristPos - pose2->wristPos)
				+ FAST_ABS(pose1->fingerPos - pose2->fingerPos)
				+ FAST_ABS(pose1->toolPos - pose2->toolPos);

	return edgeCost;
}


/**
 * [graph_add_pose description]			add a pose into active gather 
 * @param poseIndex    [description]	pose Index in lookup table
 * @param parentsIndex [description]	pose Index in lookup table ,98 start pose
 */
int32_t graph_add_activePose(uint16_t poseIndex,int16_t parentsIndex,uint32_t parentsCost)
{
	uint32_t edgeCost = 0;
	struct REACHABLE_POSE *addNode = NULL;

	addNode = (struct REACHABLE_POSE *)malloc(sizeof(struct REACHABLE_POSE));

	if ( (NULL == addNode))
	{
		return -1;
	}

	// edgeCost = FAST_ABS(poseTable[parentsIndex].shoulderPos - poseTable[poseIndex].shoulderPos)
	// 			+ FAST_ABS(poseTable[parentsIndex].armPos - poseTable[poseIndex].armPos)
	// 			+ FAST_ABS(poseTable[parentsIndex].elbowPos - poseTable[poseIndex].elbowPos)
	// 			+ FAST_ABS(poseTable[parentsIndex].wristPos - poseTable[poseIndex].wristPos)
	// 			+ FAST_ABS(poseTable[parentsIndex].fingerPos - poseTable[poseIndex].fingerPos)
	// 			+ FAST_ABS(poseTable[parentsIndex].toolPos - poseTable[poseIndex].toolPos);

	edgeCost = graph_cost_cal( poseTable + parentsIndex, poseTable + poseIndex );


	addNode -> poseIndex = poseIndex;
	addNode -> parentsIndex = parentsIndex;
	addNode -> cost = parentsCost + edgeCost;

	addNode -> next = prmGlobal.reachPoseHeader;
	prmGlobal.reachPoseHeader = addNode; 
	prmGlobal.reachPoseCnt ++;

	return 0;
}

/**
 * [graph_clr_activePose description]    anti function of graph_add_activePose
 * @return [description]
 */
int32_t graph_clr_activePose()
{
	struct REACHABLE_POSE *clrNode = NULL;

	while( NULL != prmGlobal.reachPoseHeader )
	{
		clrNode = prmGlobal.reachPoseHeader;
		prmGlobal.reachPoseHeader = prmGlobal.reachPoseHeader -> next;

		free(clrNode);
		clrNode = NULL;
		prmGlobal.reachPoseCnt -- ;
	}

	if ( 0 == prmGlobal.reachPoseCnt )
	{
		return 0;
	}
	else
	{
		return -1;
	}

}

/**
 * [graph_rank_acitvePose description]     try to rank active pose list from low cost to high cost 
 * @return [description]
 */
static int32_t graph_rank_acitivePose()
{
	struct REACHABLE_POSE *prevNode = NULL;
	struct REACHABLE_POSE *rankNode = NULL;
	struct REACHABLE_POSE *nextNode = NULL;
	

	uint32_t i = prmGlobal.reachPoseCnt;
	uint32_t j = 0;

	for ( i = prmGlobal.reachPoseCnt ;i > 0 ;i-- )
	{
		prevNode = prmGlobal.reachPoseHeader;
		rankNode = prmGlobal.reachPoseHeader -> next;
		nextNode = rankNode -> next;
		for ( j = i-2 ;j > 0;j-- )
		{

			if ( NULL == rankNode->next )
			{
				return -1;
			}

			if ( rankNode->cost > rankNode->next->cost )	//this can add more parameter
			{

				
				prevNode -> next = nextNode;
				rankNode -> next = nextNode -> next;
				nextNode -> next = rankNode;
				
				rankNode = nextNode ;	//run back
				
			}
			prevNode = rankNode;
			rankNode = rankNode -> next;
			nextNode = rankNode -> next;
		}
	}
	return 0;
}

/**
 * [graph_Is_active description]     comfire a pose is active or inactive
 * @param  searchIndex [description] pose 's index
 * @return             [description] 1:active | 0:inactive
 */
static int32_t graph_Is_active(uint16_t searchIndex)
{
	struct REACHABLE_POSE *serchNode = prmGlobal.reachPoseHeader;

	while( NULL != serchNode )
	{
		if ( serchNode->poseIndex == searchIndex )
		{
			return 1;	//the pose is already active
		}
		serchNode = serchNode -> next;
	}
	return 0;	//not a acitve pose
}

/**
 * [graph_add_final description]      add a list of final pose for the terminal of graph search
 * @param  poseIndex [description]    pose index need to add in list
 * @return           [description]    -1 fail to malloc  0 successful
 */
int32_t graph_add_final(uint16_t poseIndex)
{
	struct FINAL_POSE *node = NULL;

	node = (struct FINAL_POSE *)malloc(sizeof(struct FINAL_POSE));
	if ( (NULL == node))
	{
		return -1;
	}

	node->poseIndex = poseIndex;
	node->distance2End = FAST_ABS(prmGlobal.endConfig.shoulderPos - poseTable[poseIndex].shoulderPos)
						+ FAST_ABS(prmGlobal.endConfig.armPos - poseTable[poseIndex].armPos)
						+ FAST_ABS(prmGlobal.endConfig.elbowPos - poseTable[poseIndex].elbowPos)
						+ FAST_ABS(prmGlobal.endConfig.wristPos - poseTable[poseIndex].wristPos)
						+ FAST_ABS(prmGlobal.endConfig.fingerPos - poseTable[poseIndex].fingerPos)
						+ FAST_ABS(prmGlobal.endConfig.toolPos - poseTable[poseIndex].toolPos);

	node->next = prmGlobal.finalPoseHeader;

	prmGlobal.finalPoseHeader = node;

	return 0;
}

/**
 * [graph_clr_final description]   anti function of graph_add_final
 * @return [description]
 */
int32_t graph_clr_final()
{
	struct FINAL_POSE *node = NULL;

	while( NULL != prmGlobal.finalPoseHeader )
	{
		node = prmGlobal.finalPoseHeader;
		prmGlobal.finalPoseHeader = prmGlobal.finalPoseHeader -> next;

		free(node);
		node = NULL;
	}
	return 0;
}

/**
 * [graph_Is_final description]  comfirm a node is final
 * @param  searchIndex [description]  pose's index
 * @return             [description]  0:not a final pose | 1: is a final pose
 */
static int32_t graph_Is_final(uint16_t searchIndex)
{
	struct FINAL_POSE *serchNode = prmGlobal.finalPoseHeader;

	while( NULL != serchNode )
	{
		if( serchNode->poseIndex == searchIndex )
		{
			return 1;
		}
		serchNode = serchNode -> next;
	}
	return 0;
}



/**
 * [graph_flatten description]  flatten  the graph to find a path from start to end
 * @return [description]     1:flatten successful | -1:flatten fail
 */
int32_t graph_flatten()
{
	struct REACHABLE_POSE *flattenNode = NULL;
	struct EDGE_SAFTY_INDEX *edgeNode;

	uint16_t edgeIndex = 0;

	flattenNode = prmGlobal.reachPoseHeader;
	edgeNode = prmGlobal.saftyEdgeHeader;

	while( NULL != flattenNode )
	{
		// for ( i  = 0; i < EDGE_NUM ;i ++ )
		while( edgeNode != NULL )
		{
			edgeIndex = edgeNode->edgeIndex;


			if  ( (edgeIndexTable[edgeIndex][0] == flattenNode->poseIndex) && 
					( 0 == graph_Is_active(edgeIndexTable[edgeIndex][1]) ) )
			{
				graph_add_activePose(edgeIndexTable[edgeIndex][1],flattenNode->poseIndex,flattenNode->cost);
				if ( 1 == graph_Is_final(edgeIndexTable[edgeIndex][1]) )
				{
					prmGlobal.finalIndex = edgeIndexTable[edgeIndex][1];
					return 0; //flatten successful
				}
			}
			else if ( (edgeIndexTable[edgeIndex][1] == flattenNode->poseIndex) && 
					( 0 == graph_Is_active(edgeIndexTable[edgeIndex][0]) )
				)
			{
				graph_add_activePose(edgeIndexTable[edgeIndex][0],flattenNode->poseIndex,flattenNode->cost);
				if ( 1 == graph_Is_final(edgeIndexTable[edgeIndex][0]) )
				{
					prmGlobal.finalIndex = edgeIndexTable[edgeIndex][0];
					return 0; //flatten successful
				}
			}
			prmGlobal.saftyEdgeHeader = edgeNode->next;
			free(edgeNode);
			edgeNode = prmGlobal.saftyEdgeHeader;
				
		}

		flattenNode = flattenNode -> next;

	}

	return -1;	//flatten fail
		
		
}


/**
 * [graph_search_parents_node description]  search input pose's parents pose
 * @param  nodeIndex [description]  input index
 * @return           [description]  parents index
 * @ERROR            no found ? output = input and funciton will stack here
 */
static uint16_t graph_search_parents_node( uint16_t nodeIndex )
{
	struct REACHABLE_POSE *searchNode = prmGlobal.reachPoseHeader;

	while( searchNode != NULL )
	{
		if ( searchNode->poseIndex == nodeIndex )
		{
			nodeIndex = searchNode->parentsIndex;
			break;
		}
		searchNode = searchNode -> next;
	}

	return nodeIndex;


}

/**
 * [graph_route_map description]  create a map by flatten result
 */
int32_t graph_route_map()
{

	struct ROUTE_MAP *routeMapNode = NULL;
	uint16_t searchIndex = prmGlobal.finalIndex;

	ASSERT( prmGlobal.routeMap == NULL );

	/*** add end pose to map ***/
	routeMapNode = (struct ROUTE_MAP *)malloc(sizeof(struct ROUTE_MAP));
	if ( (NULL == routeMapNode))
	{
		return -1;
	}
	routeMapNode->poseIndex = 99;//END POSE
	routeMapNode->next = prmGlobal.routeMap;
	prmGlobal.routeMap = routeMapNode;

	/*** add tail pose to map ***/
	routeMapNode = (struct ROUTE_MAP *)malloc(sizeof(struct ROUTE_MAP));
	if ( (NULL == routeMapNode))
	{
		return -1;
	}
	routeMapNode->poseIndex = searchIndex;
	routeMapNode -> next = prmGlobal.routeMap;
	prmGlobal.routeMap = routeMapNode;	

	graph_rank_acitivePose();

	while( 98 != searchIndex )//while until start pose appear
	{
		
		searchIndex = graph_search_parents_node( searchIndex );

		routeMapNode = (struct ROUTE_MAP *)malloc(sizeof(struct ROUTE_MAP));
		if ( (NULL == routeMapNode))
		{
			return -1;
		}

		routeMapNode->poseIndex = searchIndex;
		routeMapNode -> next = prmGlobal.routeMap;
		prmGlobal.routeMap = routeMapNode;	
	}

	return 0;
}












