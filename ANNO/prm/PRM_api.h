/** ---------------------------------------------------------------------------
 * Copyright (c) 2018, Wuhan University of Technology All rights reserved.
 *
 * @file    prm_api.h
 * @date    2018-11-14
 * @version 0.1.1  2018.11
---------------------------------------------------------------------------- */

#ifndef _PRM_API_H_
#define _PRM_API_H_

#include "stdlib.h"
#include "model/anno.h"

#include "math/qformat.h"

#include "model/anno.h"
#include "PRM_config.h"
#include "prm.h"




extern struct SixD_Configure poseTable[100];
extern uint8_t edgeIndexTable[1034][2];
extern struct PRM_GLOBAL prmGlobal;

extern int32_t obstacle_get();

extern int32_t collision_chk_all_ob();
extern int32_t collision_chk_manual_one(struct SixD_Configure* pose1,struct SixD_Configure* pose2,uint32_t precision);

extern uint32_t graph_cost_cal( struct SixD_Configure* pose1,struct SixD_Configure* pose2 );
extern int32_t graph_add_activePose(uint16_t poseIndex,int16_t parentsIndex,uint32_t parentsCost);
extern int32_t graph_clr_activePose();
// extern int32_t graph_rank_acitivePose();
// extern int32_t graph_Is_active(uint16_t searchIndex);
extern int32_t graph_add_final(uint16_t poseIndex);
extern int32_t graph_clr_final();
// extern int32_t graph_Is_final(uint16_t searchIndex);
extern int32_t graph_flatten();
// extern uint16_t graph_search_node( uint16_t nodeIndex );
extern int32_t graph_route_map();

extern void prm_init();
extern int32_t prm_process();


#endif




















