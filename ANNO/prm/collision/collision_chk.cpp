/** ---------------------------------------------------------------------------
 * Copyright (c) 2018, Wuhan University of Technology All rights reserved.
 *
 * @file    collision_detect.c
 * @date    2018-11-14
 * @version 0.1.1  2018.11
---------------------------------------------------------------------------- */

/**
 * Commitucate with PL and get a safty edge
 */

#include "math/fast_math.h"


#include "prm/PRM_api.h"

#include <stdint.h>
#include "string.h"

#include "collision_register.h"

#define PRM_PL_LOGIC
//#define PRM_DDR_LOOKUP



#ifdef PRM_PL_LOGIC


#include "xil_io.h"

/************ PL api ***********************/

static void collision_chk_reg_rst()
{
	int32_t reg_value;
	UINTPTR addr = PRM_REG_CTL;

	reg_value = 0x01;
	Xil_Out32(addr, reg_value);

	reg_value = 0x00;
	Xil_Out32(addr, reg_value);

}


static void collision_chk_one_ob(uint16_t obIndex)
{
	int32_t reg_value = (int32_t) (obIndex);
	UINTPTR addr = PRM_REG_DATA;

	Xil_Out32(addr, reg_value);
}

static void collision_chk_read_out_result()
{
	UINTPTR addr;

	for( uint8_t i = 0; i < 32;i ++  )
	{
		addr = PRM_REG_READ_BASE + i*4 ;
		prmGlobal.edgeMask[i] = Xil_In32(addr);
	}
}


/************************************************************/
/*** no pineline now ***/

int32_t collision_chk_all_ob()
{
	struct OB_INDEX_NODE* nodeFree;
	struct EDGE_SAFTY_INDEX *edgeFree;

	uint16_t saftyEdgeCnt = 0;
	uint32_t edgeMaskTemp;

	uint16_t i;
	uint16_t j;

	ASSERT(prmGlobal.saftyEdgeHeader == NULL);


	collision_chk_reg_rst();

	while( prmGlobal.obIndexHeader != NULL )
	{
		nodeFree = prmGlobal.obIndexHeader;
		prmGlobal.obIndexHeader = prmGlobal.obIndexHeader -> next;

		collision_chk_one_ob(nodeFree->obIndex);

		free(nodeFree);
		nodeFree = NULL;
	}

	collision_chk_read_out_result();

	for ( i = 0; i < 32 ;i ++ )
	{
		edgeMaskTemp = prmGlobal.edgeMask[i];
		for ( j = 0; j < 32 ;j ++ )
		{		
			if ( 0 == (edgeMaskTemp & 0x00000001) )
			{
				saftyEdgeCnt ++;
			

				edgeFree = (struct EDGE_SAFTY_INDEX *)malloc(sizeof(struct EDGE_SAFTY_INDEX));
				if ( (NULL == edgeFree))
				{
					return -1;
				}

				edgeFree->edgeIndex = i*32+j;
				edgeFree->next = prmGlobal.saftyEdgeHeader;
				prmGlobal.saftyEdgeHeader = edgeFree;


			}

			edgeMaskTemp >>= 1;

		}
	}

	return saftyEdgeCnt;
}


/*------------------------------------------------------*/




// static
// int8_t collision_chk_manual_point_check(struct ThreeD_Coordinate* annoPoint,q5_t pointRadius)
// {

// 	struct OB_INDEX_NODE *checkNode = NULL;

// /****** model base ******/
// 	q5_t ob_X;
// 	q5_t ob_Y;
// 	q5_t ob_Z;

// 	ASSERT(prmGlobal.obIndexHeader != NULL);

// 	checkNode = prmGlobal.obIndexHeader;

// 	while( NULL != checkNode )
// 	{
// 		q5_t X2,Y2,Z2,R2;

// /****** model base ******/
// 		ob_X = (checkNode->obIndex) / 1024;
// 		ob_Y = (checkNode->obIndex) % 1024 / 32;
// 		ob_Z = (checkNode->obIndex) % 1024 % 32;
// 		ob_X = (512 - ob_X *32 )*32;
// 		ob_Y = (ob_Y * 32 -512 )*32;
// 		ob_Z = ob_Z * 32*32;


// 		X2 =  (ob_X - (annoPoint->X))  ;
// 		Y2 =  (ob_Y - (annoPoint->Y))  ;
// 		Z2 =  (ob_Z - (annoPoint->Z)) ;
// /****** model base ******/
// 		R2 =  (32* 32 + pointRadius)   ;

// 		if ( (X2*X2 + Y2*Y2 + Z2*Z2) > (R2*R2) )
// 		{
// 			checkNode = checkNode -> next;
// 			continue;
// 		}
// 		else 
// 		{
// 			return -1;
// 		}
// 	}

// 	return 0;

// }


// static 
// int32_t collision_chk_manual_disperse_config(struct SixD_ThreeD_POS *checkPoint)
// {
// 	struct ThreeD_Coordinate checkCoord;

// 	q5_t disperseX;
// 	q5_t disperseY;
// 	q5_t disperseZ;

// 	//check arm
	
// 	disperseX = (checkPoint->arm.X - checkPoint->shoulder.X) / (int16_t)(PRM_ANNO_ARM_PRECISION);
// 	disperseY = (checkPoint->arm.Y - checkPoint->shoulder.Y) / (int16_t)(PRM_ANNO_ARM_PRECISION);
// 	disperseZ = (checkPoint->arm.Z - checkPoint->shoulder.Z) / (int16_t)(PRM_ANNO_ARM_PRECISION);

// 	checkCoord.X = checkPoint->shoulder.X;
// 	checkCoord.Y = checkPoint->shoulder.Y;
// 	checkCoord.Z = checkPoint->shoulder.Z;

// 	for (uint32_t i = 0; i < PRM_ANNO_ARM_PRECISION ;i++)
// 	{
// 		checkCoord.X += disperseX;
// 		checkCoord.Y += disperseY;
// 		checkCoord.Z += disperseZ;


// 		if ( 0 == collision_chk_manual_point_check(&checkCoord,70*32) )
// 		{

// 		}
// 		else 
// 		{
// 			return -1;
// 		}
// 	}

// 	disperseX = (checkPoint->wrist.X - checkPoint->arm.X) / (int16_t)(PRM_ANNO_WRIST_PRECISION);
// 	disperseY = (checkPoint->wrist.Y - checkPoint->arm.Y) / (int16_t)(PRM_ANNO_WRIST_PRECISION);
// 	disperseZ = (checkPoint->wrist.Z - checkPoint->arm.Z) / (int16_t)(PRM_ANNO_WRIST_PRECISION);

// 	checkCoord.X = checkPoint->arm.X;
// 	checkCoord.Y = checkPoint->arm.Y;
// 	checkCoord.Z = checkPoint->arm.Z;

// 	//check wrist
// 	for(uint32_t i = 0; i < PRM_ANNO_WRIST_PRECISION; i++ )
// 	{
// 		checkCoord.X += disperseX;
// 		checkCoord.Y += disperseY;
// 		checkCoord.Z += disperseZ;

// 		if ( 0 == collision_chk_manual_point_check(&checkCoord,5*32) )
// 		{
// 			;
// 		}
// 		else 
// 		{
// 			return -1;
// 		}
// 	}

// 	disperseX = (checkPoint->tool.X - checkPoint->wrist.X) / (int16_t)(PRM_ANNO_TOOL_PRECISION);
// 	disperseY = (checkPoint->tool.Y - checkPoint->wrist.Y) / (int16_t)(PRM_ANNO_TOOL_PRECISION);
// 	disperseZ = (checkPoint->tool.Z - checkPoint->wrist.Z) / (int16_t)(PRM_ANNO_TOOL_PRECISION);

// 	checkCoord.X = checkPoint->wrist.X;
// 	checkCoord.Y = checkPoint->wrist.Y;
// 	checkCoord.Z = checkPoint->wrist.Z;

// 	//check tool
// 	for (uint32_t i = 0; i < PRM_ANNO_TOOL_PRECISION; i++ )
// 	{
// 		checkCoord.X += disperseX;
// 		checkCoord.Y += disperseY;
// 		checkCoord.Z += disperseZ;

// 		if ( 0 == collision_chk_manual_point_check(&checkCoord,3*32) )
// 		{
// 			;
// 		}
// 		else 
// 		{
// 			return -1;
// 		}
// 	}

// 	return 0;
	
// }



// /**
//  * [collision_manual description] check whether one additional edge is collision free
//  * @param  pose1 [description]	one pose
//  * @param  pose2 [description]  the other pose
//  * @return       [description]  free or not free
//  */
// int32_t collision_chk_manual_one(struct SixD_Configure* pose1,struct SixD_Configure* pose2,uint32_t precision)
// {
// 	struct SixD_Configure disperseConfig;
// 	struct SixD_ThreeD_POS checkPoint;
	
// 	q6_t disperseS;
// 	q6_t disperseA;
// 	q6_t disperseE;
// 	q6_t disperseW;
// 	q6_t disperseF;
// 	q6_t disperseT;

//  	memcpy(&disperseConfig,pose1,sizeof(struct SixD_Configure));

// 	disperseS = (pose2->shoulderPos - pose1->shoulderPos) / (int16_t)(precision);
// 	disperseA = (pose2->armPos - pose1->armPos) / (int16_t)(precision);
// 	disperseE = (pose2->elbowPos - pose1->elbowPos) / (int16_t)(precision);
// 	disperseW = (pose2->wristPos - pose1->wristPos) / (int16_t)(precision);
// 	disperseF = (pose2->fingerPos - pose1->fingerPos) / (int16_t)(precision);
// 	disperseT = (pose2->toolPos - pose1->toolPos) / (int16_t)(precision);

// 	for ( uint32_t i = 0; i < precision;i++ )
// 	{
// 		disperseConfig.shoulderPos += disperseS;
// 		disperseConfig.armPos += disperseA;
// 		disperseConfig.elbowPos += disperseE;
// 		disperseConfig.wristPos += disperseW;
// 		disperseConfig.fingerPos += disperseF;
// 		disperseConfig.toolPos += disperseT;

// 		disperse_motion_model( &disperseConfig,&checkPoint );

// 		if ( 0 == collision_chk_manual_disperse_config(&checkPoint) )
// 		{
// 			;
// 		}
// 		else 
// 		{
// 			return -1;
// 		}
// 	}
// 	return 0;
// }



















#endif

#ifdef PRM_DDR_LOOKUP

#endif



