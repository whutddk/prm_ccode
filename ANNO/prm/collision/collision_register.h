/** ---------------------------------------------------------------------------
 * Copyright (c) 2018, Wuhan University of Technology All rights reserved.
 *
 * @file    prm_register.h
 * @date    2018-11-14
 * @version 0.1.1  2018.11
---------------------------------------------------------------------------- */

#ifndef _PRM_REGISTER_H_
#define _PRM_REGISTER_H_



#define PRM_PL_LOGIC

#ifdef PRM_PL_LOGIC


#define PRM_REG_BASE 	0x43C10000
#define PRM_REG_DATA 	(PRM_REG_BASE)
#define PRM_REG_CTL		(PRM_REG_BASE | 4*1)
#define PRM_REG_READ_BASE (PRM_REG_BASE | 4*10)


#endif





#endif




