/** ---------------------------------------------------------------------------
 * Copyright (c) 2018, PS-Micro, Co. Ltd.  All rights reserved.
 *
 * @file    anno.c
 * @date    2018-11-27
 * @version 0.1.1  2018.11
---------------------------------------------------------------------------- */



#include <stdint.h>

#include "math/fast_math.h"
#include "math/sin_table.h"
#include "anno.h"


#include "math.h"



#include "middleware/stepper/src/steppers.h"
#include "board/drivers/freecars/freecars.h"



/**
 * As a non pid control model,a timer ticker is used to control all 6 axis's speed
 * 
 */

// struct ANNO_POS *annoCoordinate = NULL;


// void anno_task(void)
// {

// }

/**
 * add a 6D coordinate into the linklist
 * 
 */
// void anno_coordinate_add(struct SixD_Configure *coordAdd)
// {
// 	struct ANNO_POS *node = NULL;
// 	struct ANNO_POS *pointer = annoCoordinate;

// 	node = (struct ANNO_POS *)malloc(sizeof(struct ANNO_POS));


// 	if ( (NULL == node))
// 	{
// 		//print("error!\r\n");
// 		while(1);
// 	}

// 	node -> SixD_Cfg.shoulderPos = coordAdd -> shoulderPos;
// 	node -> SixD_Cfg.armPos = coordAdd -> armPos;
// 	node -> SixD_Cfg.elbowPos = coordAdd -> elbowPos;
// 	node -> SixD_Cfg.wristPos = coordAdd -> wristPos;
// 	node -> SixD_Cfg.fingerPos = coordAdd -> fingerPos;
// 	node -> SixD_Cfg.toolPos = coordAdd -> toolPos;

// 	node -> next = NULL;

// 	//find the tail of linklist
// 	while ( pointer -> next != NULL )
// 	{
// 		pointer = pointer -> next; 
// 	}

// 	pointer -> next = node;
// }

/**
 * load a coordinate from linklist and them delete it 
 * 
 */
// void anno_coordinate_load(struct SixD_Configure *coordLoad)
// {
// 	struct ANNO_POS *pointer = annoCoordinate;

// 	coordLoad -> shoulderPos = annoCoordinate -> SixD_Cfg.shoulderPos;
// 	coordLoad -> armPos = annoCoordinate -> SixD_Cfg.armPos;
// 	coordLoad -> elbowPos = annoCoordinate -> SixD_Cfg.elbowPos;
// 	coordLoad -> wristPos = annoCoordinate -> SixD_Cfg.wristPos;
// 	coordLoad -> fingerPos = annoCoordinate -> SixD_Cfg.fingerPos;
// 	coordLoad -> toolPos = annoCoordinate -> SixD_Cfg.toolPos;

// 	annoCoordinate = annoCoordinate -> next;

// 	free(pointer);
// }


/**
 * define coordinate road map as a Singly linked list
 * 
 */
// int32_t anno_coordinate_init()
// {

	// annoCoordinate = (struct ANNO_POS *)malloc(sizeof(struct ANNO_POS));


	// if ( (NULL == annoCoordinate))

	// {
	// 	//print("error!\r\n");
	// 	return -1;
	// }

	// annoCoordinate -> SixD_Cfg.shoulderPos = 0;
	// annoCoordinate -> SixD_Cfg.armPos = 0;
	// annoCoordinate -> SixD_Cfg.elbowPos = 0;
	// annoCoordinate -> SixD_Cfg.wristPos = 0;
	// annoCoordinate -> SixD_Cfg.fingerPos = 0;
	// annoCoordinate -> SixD_Cfg.toolPos = 0;

	// annoCoordinate -> SixD_Register.AXIS0_REG = 0x7FFFFFFF;
	// annoCoordinate -> SixD_Register.AXIS1_REG = 0x7FFFFFFF;
	// annoCoordinate -> SixD_Register.AXIS2_REG = 0x7FFFFFFF;
	// annoCoordinate -> SixD_Register.AXIS3_REG = 0x7FFFFFFF;
	// annoCoordinate -> SixD_Register.AXIS4_REG = 0x7FFFFFFF;
	// annoCoordinate -> SixD_Register.AXIS5_REG = 0x7FFFFFFF;

	// annoCoordinate -> next = NULL;

// }





/**
*	input axis number :0~6 and angle present in degree
*	output angle present in uint32_t which can push into PL
*/

void POS_cal(struct SixD_Configure *SixD_Cfg , uint32_t *sixDRegister)
{

	if ( ( SixD_Cfg -> shoulderPos > -180*64) && ( SixD_Cfg -> shoulderPos < 180*64) ) { ; }
	else if (SixD_Cfg -> shoulderPos < -180*64 ) { SixD_Cfg -> shoulderPos = -180*64; }
	else if ( SixD_Cfg -> shoulderPos > 180*64 ) { SixD_Cfg -> shoulderPos = 180*64; }
	sixDRegister[0] = (uint32_t)((int32_t)(32 * ( SixD_Cfg -> shoulderPos / 64. / 1.8) * ( 27. / 4. )) + 0x7fffffff);

	if ( ( SixD_Cfg -> armPos > -90*64) && ( SixD_Cfg -> armPos < 90*64) ) { ; }
	else if ( SixD_Cfg -> armPos < -90*64 ) { SixD_Cfg -> armPos = -90*64; }
	else if ( SixD_Cfg -> armPos > 90*64 ) { SixD_Cfg -> armPos = 90*64; }
	sixDRegister[1] = (uint32_t)((int32_t)(32 * ( SixD_Cfg -> armPos / 64. / 1.8) * ( 123. / 8. )) + 0x7fffffff);

	if ( ( SixD_Cfg -> elbowPos > -90*64) && ( SixD_Cfg -> elbowPos < 90*64) ) { ; }
	else if ( SixD_Cfg -> elbowPos < -90*64 ) { SixD_Cfg -> elbowPos = -90*64; }
	else if ( SixD_Cfg -> elbowPos > 90*64 ) { SixD_Cfg -> elbowPos = 90*64; }
	sixDRegister[2] = (uint32_t)( (int32_t)(32 * ( SixD_Cfg -> elbowPos / 64. / 1.8) * ( 45 / 4. )) + 0x7fffffff);

	if ( ( SixD_Cfg -> wristPos > -180*64) && ( SixD_Cfg -> wristPos < 180*64) ) { ; }
	else if ( SixD_Cfg -> wristPos < -180*64 ) { SixD_Cfg -> wristPos = -180*64; }
	else if ( SixD_Cfg -> wristPos > 180*64 ) { SixD_Cfg -> wristPos = 180*64; }
	sixDRegister[3] = (uint32_t)( (int32_t)(32 * ( SixD_Cfg -> wristPos / 64. / 1.8) * ( 225 / 16. )) + 0x7fffffff);

	if ( ( SixD_Cfg -> fingerPos > -90*64) && ( SixD_Cfg -> fingerPos < 90*64) ) { ; }
	else if ( SixD_Cfg -> fingerPos < -90*64 ) { SixD_Cfg -> fingerPos = -90*64; }
	else if ( SixD_Cfg -> fingerPos > 90*64 ) { SixD_Cfg -> fingerPos = 90*64; }
	sixDRegister[4] = (uint32_t)( (int32_t)(32 * ( SixD_Cfg -> fingerPos / 64. / 1.8) * ( 165 / 16. )) + 0x7fffffff);


	if ( ( SixD_Cfg -> toolPos > -180*64) && ( SixD_Cfg -> toolPos < 180*64) ) { ; }
	else if ( SixD_Cfg -> toolPos < -180*64 ) { SixD_Cfg -> toolPos = -180*64; }
	else if ( SixD_Cfg -> toolPos > 180*64 ) { SixD_Cfg -> toolPos = 180*64; }
	sixDRegister[5] = (uint32_t)((int32_t)(32 * ( SixD_Cfg -> toolPos / 64. / 1.8) ) + 0x7fffffff);

	// if ( AUTO_UPDATE )
	// {
	// 	write_steppers_position_cmd( 0, sixD_register -> AXIS0_REG );
	// 	write_steppers_position_cmd( 1, sixD_register -> AXIS1_REG );
	// 	write_steppers_position_cmd( 2, sixD_register -> AXIS2_REG );
	// 	write_steppers_position_cmd( 3, sixD_register -> AXIS3_REG );
	// 	write_steppers_position_cmd( 4, sixD_register -> AXIS4_REG );
	// 	write_steppers_position_cmd( 5, sixD_register -> AXIS5_REG );
	// }

}

// void Div_cal(struct SixD_REG *start_pos,struct SixD_REG *end_pos,struct SixD_REG *speed_setting,uint8_t AUTO_UPDATE)
// {
// 	//return (PL_BASE_CLK / CTL_FRE / fast_abs(end_pos - start_pos));
	
// //	speed_setting . AXIS0_REG = (PL_BASE_CLK / CTL_FRE / fast_abs(end_pos . AXIS0_REG - start_pos . AXIS0_REG));
// //	speed_setting . AXIS1_REG = (PL_BASE_CLK / CTL_FRE / fast_abs(end_pos . AXIS1_REG - start_pos . AXIS1_REG));
// //	speed_setting . AXIS2_REG = (PL_BASE_CLK / CTL_FRE / fast_abs(end_pos . AXIS2_REG - start_pos . AXIS2_REG));
// //	speed_setting . AXIS3_REG = (PL_BASE_CLK / CTL_FRE / fast_abs(end_pos. AXIS3_REG - start_pos . AXIS3_REG));
// //	speed_setting . AXIS4_REG = (PL_BASE_CLK / CTL_FRE / fast_abs(end_pos . AXIS4_REG - start_pos . AXIS4_REG));
// //	speed_setting . AXIS5_REG = (PL_BASE_CLK / CTL_FRE / fast_abs(end_pos. AXIS5_REG - start_pos . AXIS5_REG));

// 	if ( end_pos->AXIS0_REG == start_pos->AXIS0_REG )
// 	{
// 		speed_setting->AXIS0_REG = STEPPERS_DIV_MAX;
// 	}
// 	else
// 	{
// 		speed_setting -> AXIS0_REG = (PL_BASE_CLK *DEBUG_SECOND / fast_abs(end_pos -> AXIS0_REG - start_pos -> AXIS0_REG));
// 	}

// 	if ( end_pos -> AXIS1_REG == start_pos->AXIS1_REG )
// 	{
// 		speed_setting -> AXIS1_REG = STEPPERS_DIV_MAX;
// 	}
// 	else
// 	{
// 		speed_setting -> AXIS1_REG = (PL_BASE_CLK *DEBUG_SECOND / fast_abs(end_pos -> AXIS1_REG - start_pos -> AXIS1_REG));
// 	}
	
// 	if ( end_pos->AXIS2_REG == start_pos->AXIS2_REG )
// 	{
// 		speed_setting -> AXIS2_REG = STEPPERS_DIV_MAX;
// 	}
// 	else
// 	{
// 		speed_setting -> AXIS2_REG = (PL_BASE_CLK *DEBUG_SECOND / fast_abs(end_pos -> AXIS2_REG - start_pos -> AXIS2_REG));
// 	}

// 	if ( end_pos->AXIS3_REG == start_pos->AXIS3_REG )
// 	{
// 		speed_setting -> AXIS3_REG = STEPPERS_DIV_MAX;
// 	}
// 	else
// 	{
// 		speed_setting -> AXIS3_REG = (PL_BASE_CLK *DEBUG_SECOND / fast_abs(end_pos -> AXIS3_REG - start_pos -> AXIS3_REG));
// 	}

// 	if ( end_pos->AXIS4_REG == start_pos->AXIS4_REG )
// 	{
// 		speed_setting -> AXIS4_REG = STEPPERS_DIV_MAX;
// 	}
// 	else
// 	{
// 		speed_setting -> AXIS4_REG = (PL_BASE_CLK *DEBUG_SECOND / fast_abs(end_pos -> AXIS4_REG - start_pos -> AXIS4_REG));
// 	}

// 	if ( end_pos -> AXIS5_REG == start_pos -> AXIS5_REG )
// 	{
// 		speed_setting -> AXIS5_REG = STEPPERS_DIV_MAX;
// 	}
// 	else
// 	{
// 		speed_setting -> AXIS5_REG = (PL_BASE_CLK *DEBUG_SECOND / fast_abs(end_pos -> AXIS5_REG - start_pos -> AXIS5_REG));
// 	}

// 	if ( AUTO_UPDATE )
// 	{
// 		write_steppers_speed_cmd( 0, speed_setting -> AXIS0_REG );
// 		write_steppers_speed_cmd( 1, speed_setting -> AXIS1_REG );
// 		write_steppers_speed_cmd( 2, speed_setting -> AXIS2_REG );
// 		write_steppers_speed_cmd( 3, speed_setting -> AXIS3_REG );
// 		write_steppers_speed_cmd( 4, speed_setting -> AXIS4_REG );
// 		write_steppers_speed_cmd( 5, speed_setting -> AXIS5_REG );
// 	}

// }

/**
 * calculate all check point XYZ coordinate by 6D angle
 * [input] 6D angle 
 * [output] 6*3D XYZ coordinate 
 */
void disperse_motion_model(struct SixD_Configure *sixCoord,struct SixD_ThreeD_POS *SixThreePosition )
{

	sq15_t s1,c1,s2,c2,s3,c3,s4,c4,s5,c5,s6,c6;

	 
	s1 = search_sin( sixCoord -> shoulderPos );
	c1 = search_cos( sixCoord -> shoulderPos );

	s2 = search_sin( sixCoord -> armPos );
	c2 = search_cos( sixCoord -> armPos );

	s3 = search_sin( sixCoord -> elbowPos );
	c3 = search_cos( sixCoord -> elbowPos );

	s4 = search_sin( sixCoord -> wristPos );
	c4 = search_cos( sixCoord -> wristPos );

	s5 = search_sin( sixCoord -> fingerPos );
	c5 = search_cos( sixCoord -> fingerPos );

	s6 = search_sin( sixCoord -> toolPos );
	c6 = search_cos( sixCoord -> toolPos );

	sq15_t s1c2 = (sq15_t)((s1*c2) >> 15);
	sq15_t c1c2 = (sq15_t)((c1*c2) >> 15);

	sq15_t s1s2 = (sq15_t)((s1 *s2) >> 15);
	sq15_t c1s2 = (sq15_t)((c1 *s2) >> 15);

	sq15_t s1c2s3 = (sq15_t)((s1c2 * s3) >> 15);
	sq15_t s1s2c3 = (sq15_t)((s1s2 *c3) >> 15);
	sq15_t c1c2s3 = (sq15_t)((c1c2 *s3) >> 15);
	sq15_t c1s2c3 = (sq15_t)((c1s2 *c3) >> 15);

	sq15_t c2c3 = (sq15_t)((c2*c3) >> 15);
	sq15_t s2s3 = (sq15_t)((s2 *s3) >> 15);

	sq15_t c1s4 = (sq15_t)((c1*s4) >> 15);
	sq15_t s1s4 = (sq15_t)((s1*s4) >> 15);
	sq15_t c4s5 = (sq15_t)((c4*s5) >> 15);
	sq15_t c2s3 = (sq15_t)((c2*s3) >> 15);
	sq15_t s2c3 = (sq15_t)((s2*c3) >> 15);

	sq15_t c4c5 = (sq15_t)((c4*c5) >> 15);

	sq15_t s1s2s3 = (sq15_t)((s1*s2s3) >> 15);
	sq15_t s1c2c3 = (sq15_t)((s1c2*c3) >> 15);
	sq15_t c1s2s3 = (sq15_t)((c1*s2s3) >> 15);
	sq15_t c1c2c3 = (sq15_t)((c1 *c2c3) >> 15);

	sq15_t c1s4s5 = (sq15_t)((c1s4*s5) >> 15);
	sq15_t s1s2s3c4s5 = (sq15_t)((s1s2s3*c4s5) >> 15);
	sq15_t s1c2c3c4s5 = (sq15_t)((s1c2c3*c4s5) >> 15);

	sq15_t s1s4s5 = (sq15_t)((s1s4*s5) >> 15);
	sq15_t c1s2s3c4s5 = (sq15_t)((c1s2s3*c4s5) >> 15);

	sq15_t c1c2c3c4s5 = (sq15_t)((c1c2c3*c4s5) >> 15);
	sq15_t s1c2s3c5 = (sq15_t)((s1c2s3*c5) >> 15);

	sq15_t s1s2c3c5 = (sq15_t)((s1s2c3*c5) >> 15);
	sq15_t c1s2c3c5 = (sq15_t)((c1s2c3*c5) >> 15);
	sq15_t c1c2s3c5 = (sq15_t)((c1c2s3*c5) >> 15);

	sq15_t c2c3c5 = (sq15_t)((c2c3*c5) >> 15);
	sq15_t s2s3c5 = (sq15_t)((s2s3*c5) >> 15);
	sq15_t c2s3c4s5 = (sq15_t)((c2s3*c4s5) >> 15);
	sq15_t s2c3c4s5 = (sq15_t)((s2c3*c4s5) >> 15);


	sq15_t c4c6 = (sq15_t)((c4*c6) >> 15);
	sq15_t c1c4c6 = (sq15_t)((c1*c4c6) >> 15);
	sq15_t s1c4c6 = (sq15_t)((s1*c4c6) >> 15);
	sq15_t s4c6 = (sq15_t)((s4*c6) >> 15);
	sq15_t c5s6 = (sq15_t)((c5*s6) >> 15);

	sq15_t s5s6 = (sq15_t)((s5*s6) >> 15);
	sq15_t s5c6 = (sq15_t)((s5*c6) >> 15);

	sq15_t c2c3s5s6 = (sq15_t)((c2c3*s5s6) >> 15);
	sq15_t s1s2s3s4c6 = (sq15_t)((s1s2s3*s4c6) >> 15);
	sq15_t c1s2s3s4c6 = (sq15_t)((c1s2s3*s4c6) >> 15);
	sq15_t s2s3s5s6 = (sq15_t)((s2s3*s5s6) >> 15);
	sq15_t s1c2c3s4c6 = (sq15_t)((s1c2c3*s4c6) >> 15);
	sq15_t c1c2c3s4c6 = (sq15_t)((c1c2c3*s4c6) >> 15);

	sq15_t c4c5c6 = (sq15_t)((c4c5*c6) >> 15);
	sq15_t c4c5s6 = (sq15_t)((c4c5*s6) >> 15);

	sq15_t c1s4c5s6 = (sq15_t)((c1s4*c5s6) >> 15);
	sq15_t s1s4c5s6 = (sq15_t)((s1s4*c5s6) >> 15);

	sq15_t c2s3c4c5s6 = (sq15_t)((c2s3*c4c5s6) >> 15);
	sq15_t s2c3c4c5s6 = (sq15_t)((s2c3*c4c5s6) >> 15);

	sq15_t s1s2s3c4c5s6 = (sq15_t)((s1s2s3*c4c5s6) >> 15);
	sq15_t c1s2s3c4c5s6 = (sq15_t)((c1s2s3*c4c5s6) >> 15);

	sq15_t s1c2c3c4c5s6 = (sq15_t)((s1c2c3*c4c5s6) >> 15);
	sq15_t c1c2c3c4c5s6 = (sq15_t)((c1c2c3*c4c5s6) >> 15);

	sq15_t s1c2s3s5s6 = (sq15_t)((s1c2s3*s5s6) >> 15);
	sq15_t c1c2s3s5s6 = (sq15_t)((c1c2s3*s5s6) >> 15);

	sq15_t s1s2c3s5s6 = (sq15_t)((s1s2c3*s5s6) >> 15);
	sq15_t c1s2c3s5s6 = (sq15_t)((c1s2c3*s5s6) >> 15);

	sq15_t c2s3s4c6 = (sq15_t)((c2s3*s4c6) >> 15);
	sq15_t s2c3s4c6 = (sq15_t)((s2c3*s4c6) >> 15);

	sq15_t c2c3s5c6 = (sq15_t)((c2c3*s5c6) >> 15);
	sq15_t s2s3s5c6 = (sq15_t)((s2s3*s5c6) >> 15);

	sq15_t c2s3c4c5c6 = (sq15_t)((c2s3*c4c5c6) >> 15);
	sq15_t s2c3c4c5c6 = (sq15_t)((s2c3*c4c5c6) >> 15);

	sq15_t c4s6 = (sq15_t)((c4*s6) >> 15);
	sq15_t s4s6 = (sq15_t)((s4*s6) >> 15);

	sq15_t c5c6 = (sq15_t)((c5*c6) >> 15);

	sq15_t c1c4s6 = (sq15_t)((c1*c4s6) >> 15);
	sq15_t s1c4s6 = (sq15_t)((s1*c4s6) >> 15);

	sq15_t s1s2s3s4s6 = (sq15_t)((s1s2s3*s4s6) >> 15);
	sq15_t c1s2s3s4s6 = (sq15_t)((c1s2s3*s4s6) >> 15);

	sq15_t c2s3s4s6 = (sq15_t)((c2s3*s4s6) >> 15);
	sq15_t s2c3s4s6 = (sq15_t)((s2c3*s4s6) >> 15);

	sq15_t s1c2c3s4s6 = (sq15_t)((s1c2c3*s4s6) >> 15);
	sq15_t c1c2c3s4s6 = (sq15_t)((c1c2c3*s4s6) >> 15);

	sq15_t c1s4c5c6 = (sq15_t)((c1s4*c5c6) >> 15);
	sq15_t s1s4c5c6 = (sq15_t)((s1s4*c5c6) >> 15);

	sq15_t s1s2s3c4c5c6 = (sq15_t)((s1s2s3*c4c5c6) >> 15);
	sq15_t c1s2s3c4c5c6 = (sq15_t)((c1s2s3*c4c5c6) >> 15);

	sq15_t s1c2c3c4c5c6 = (sq15_t)((s1c2c3*c4c5c6) >> 15);
	sq15_t c1c2c3c4c5c6 = (sq15_t)((c1c2c3*c4c5c6) >> 15);

	sq15_t s1c2s3s5c6 = (sq15_t)((s1c2s3*s5c6) >> 15);
	sq15_t c1c2s3s5c6 = (sq15_t)((c1c2s3*s5c6) >> 15);

	sq15_t s1s2c3s5c6 = (sq15_t)((s1s2c3*s5c6) >> 15);
	sq15_t c1s2c3s5c6 = (sq15_t)((c1s2c3*s5c6) >> 15);
/**************************************************************************/

	SixThreePosition -> shoulder.X = (q5_t)(0);			//Q5
	SixThreePosition -> shoulder.Y = (q5_t)0;			//Q5
	SixThreePosition -> shoulder.Z = (q5_t)(264*32);	//Q5

	SixThreePosition -> arm.X = (q5_t)((-225*s1c2) >> 10);
	SixThreePosition -> arm.Y = (q5_t)((225*c1c2) >> 10);
	SixThreePosition -> arm.Z = (q5_t)((225*s2)>>10) + (q5_t)(264*32);


	// SixThreePosition -> elbow.X = SixThreePosition -> arm.X - 80*s1c2s3 - 80*s1s2c3;
	// SixThreePosition -> elbow.Y = SixThreePosition -> arm.Y + 80*c1c2s3 + 80*c1s2c3;
	// SixThreePosition -> elbow.Z = SixThreePosition -> arm.Z - 80*c2c3 + 80*s2s3;


	SixThreePosition -> wrist.X = SixThreePosition -> arm.X + (q5_t)((-217*(s1c2s3 + s1s2c3)) >> 10);
	SixThreePosition -> wrist.Y = SixThreePosition -> arm.Y + (q5_t)(( 217*(c1c2s3 + c1s2c3)) >> 10);
	SixThreePosition -> wrist.Z = SixThreePosition -> arm.Z + (q5_t)(( 217*(s2s3 - c2c3 )) >> 10) ;


	SixThreePosition -> finger.X = SixThreePosition -> wrist.X + (q5_t)((40*(-c1s4s5 + s1s2s3c4s5 - s1c2c3c4s5 - s1c2s3c5 - s1s2c3c5)) >> 10);
	SixThreePosition -> finger.Y = SixThreePosition -> wrist.Y + (q5_t)((40*(-s1s4s5 - c1s2s3c4s5 + c1c2c3c4s5 + c1c2s3c5 + c1s2c3c5)) >> 10);
	SixThreePosition -> finger.Z = SixThreePosition -> wrist.Z + (q5_t)((40*(-c2c3c5 + s2s3c5 + c2s3c4s5 + s2c3c4s5)) >> 10) ;


	SixThreePosition -> tool.X = SixThreePosition -> finger.X + (q5_t)((TOOL_X * (c1c4c6 + s1s2s3s4c6 - s1c2c3s4c6 - c1s4c5s6 + s1s2s3c4c5s6 - s1c2c3c4c5s6 + s1c2s3s5s6 + s1s2c3s5s6) + TOOL_Y *(c1c4s6 + s1s2s3s4s6 - s1c2c3s4s6 + c1s4c5c6 - s1s2s3c4c5c6 + s1c2c3c4c5c6 - s1c2s3s5c6 - s1s2c3s5c6) - TOOL_Z *(c1s4s5 - s1s2s3c4s5 + s1c2c3c4s5 + s1c2s3c5 + s1s2c3c5)) >> 10);
	SixThreePosition -> tool.Y = SixThreePosition -> finger.Y + (q5_t)((TOOL_X * (s1c4c6 - c1s2s3s4c6 + c1c2c3s4c6 - s1s4c5s6 - c1s2s3c4c5s6 + c1c2c3c4c5s6 - c1c2s3s5s6 - c1s2c3s5s6) + TOOL_Y *(s1c4s6 - c1s2s3s4s6 + c1c2c3s4s6 + s1s4c5c6 + c1s2s3c4c5c6 - c1c2c3c4c5c6 + c1c2s3s5c6 + c1s2c3s5c6) - TOOL_Z *(s1s4s5 + c1s2s3c4s5 - c1c2c3c4s5 - c1c2s3c5 - c1s2c3c5)) >> 10);
	SixThreePosition -> tool.Z = SixThreePosition -> finger.Z + (q5_t)((TOOL_X * (c2c3s5s6 - s2s3s5s6 + c2s3c4c5s6 + s2c3c4c5s6 + c2s3s4c6 + s2c3s4c6) - TOOL_Y *(c2c3s5c6 - s2s3s5c6 + c2s3c4c5c6 + s2c3c4c5c6 - c2s3s4s6 - s2c3s4s6) - TOOL_Z *(c2c3c5 - s2s3c5 - c2s3c4s5 - s2c3c4s5)) >> 10);


//	push(0,(int16_t)(SixThreePosition -> shoulder.X/32.*10));
//	push(1,(int16_t)(SixThreePosition -> shoulder.Y/32.*10));
//	push(2,(int16_t)(SixThreePosition -> shoulder.Z/32.*10));
//
//	push(3,(int16_t)(SixThreePosition -> arm.X/32.*10));
//	push(4,(int16_t)(SixThreePosition -> arm.Y/32.*10));
//	push(5,(int16_t)(SixThreePosition -> arm.Z/32.*10));
//
//	push(6,(int16_t)(SixThreePosition -> elbow.X/32.*10));
//	push(7,(int16_t)(SixThreePosition -> elbow.Y/32.*10));
//	push(8,(int16_t)(SixThreePosition -> elbow.Z/32.*10));
//
//	push( 9,(int16_t)(SixThreePosition -> wrist.X/32.*10));
//	push(10,(int16_t)(SixThreePosition -> wrist.Y/32.*10));
//	push(11,(int16_t)(SixThreePosition -> wrist.Z/32.*10));
//
//	push(12,(int16_t)(SixThreePosition -> finger.X/32.*10));
//	push(13,(int16_t)(SixThreePosition -> finger.Y/32.*10));
//	push(14,(int16_t)(SixThreePosition -> finger.Z/32.*10));
//
//	push(15,(int16_t)(SixThreePosition -> tool.X/32.*10));
//	push(16,(int16_t)(SixThreePosition -> tool.Y/32.*10));
//	push(17,(int16_t)(SixThreePosition -> tool.Z/32.*10));

}


// void step2position(struct SixD_Configure *SixD_Cfg)
// {
	
// 	struct SixD_REG posOutputReg;
// 	struct SixD_REG speedOutputReg;

// 	POS_cal( SixD_Cfg , &posOutputReg , 1 );
// 	Div_cal( &(annoCoordinate->SixD_Register), &posOutputReg, &speedOutputReg , 1 );

// 	annoCoordinate->SixD_Register.AXIS0_REG = posOutputReg.AXIS0_REG;
// 	annoCoordinate->SixD_Register.AXIS1_REG = posOutputReg.AXIS1_REG;
// 	annoCoordinate->SixD_Register.AXIS2_REG = posOutputReg.AXIS2_REG;
// 	annoCoordinate->SixD_Register.AXIS3_REG = posOutputReg.AXIS3_REG;
// 	annoCoordinate->SixD_Register.AXIS4_REG = posOutputReg.AXIS4_REG;
// 	annoCoordinate->SixD_Register.AXIS5_REG = posOutputReg.AXIS5_REG;
// }
