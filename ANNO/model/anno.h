/** ---------------------------------------------------------------------------
 * Copyright (c) 2018, PS-Micro, Co. Ltd.  All rights reserved.
 *
 * @file    anno.h
 * @date    2018-10-13
 * @version 0.1.1  2018.10
---------------------------------------------------------------------------- */

#ifndef _ANNO_H_
#define _ANNO_H_

#include <stdint.h>

#include "math/qformat.h"




#define PI 3.141592653
#define PI_Q6 201



/********************* Tool Kit **************************************/

#define TOOL_EMPTY
//#define TOOL_FLAG
//#define TOOL_DRILL
//#define TOOL_COMPASS

#ifdef TOOL_EMPTY
#define TOOL_X 		0
#define TOOL_Y 		0
#define TOOL_Z 		0
#endif


struct ThreeD_Coordinate{
	q5_t X;
	q5_t Y;
	q5_t Z;
};

//present angle 
struct SixD_Configure{
	q6_t shoulderPos;
	q6_t armPos;
	q6_t elbowPos;
	q6_t wristPos;
	q6_t fingerPos;
	q6_t toolPos;
};




struct ANNO_POS{

	struct SixD_Configure SixD_Cfg;
	uint32_t sixDRegister[6];

	struct ANNO_POS* next;

};

struct SixD_ThreeD_POS{
	struct ThreeD_Coordinate shoulder;
	struct ThreeD_Coordinate arm;
	struct ThreeD_Coordinate elbow;
	struct ThreeD_Coordinate wrist;
	struct ThreeD_Coordinate finger;
	struct ThreeD_Coordinate tool;
};

// extern struct ANNO_POS *annoCoordinate;




// extern void anno_task(void);
// extern void anno_coordinate_add(struct SixD_Configure *coordAdd);
// extern void anno_coordinate_load(struct SixD_Configure *coordLoad);
// extern int32_t anno_coordinate_init();
extern void POS_cal(struct SixD_Configure *SixD_Cfg , uint32_t *sixDRegister);
extern void disperse_motion_model(struct SixD_Configure *sixCoord,struct SixD_ThreeD_POS *SixThreePosition );



#endif



