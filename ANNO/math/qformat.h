/** ---------------------------------------------------------------------------
 * Copyright (c) 2018, PS-Micro, Co. Ltd.  All rights reserved.
 *
 * @file    qformat.h
 * @date    2018-10-25
 * @version 0.1.1  2018.10
---------------------------------------------------------------------------- */


#ifndef _QFORMAT_H_
#define _QFORMAT_H_

#include <stdint.h>

//XYZ
typedef int16_t q5_t;		//-1024~1023   0.03
//angle
typedef  int16_t q6_t;		//-512~511   0.0156
//sincos
typedef int16_t sq15_t;





#endif

