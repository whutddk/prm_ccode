

#include <stdio.h>
#include "xparameters.h"
#include "xgpiops.h"
#include "xscugic.h"
#include "xscutimer.h"
#include "xil_exception.h"
#include "xplatform_info.h"

#define TIMER_DEVICE_ID (XPAR_XSCUTIMER_0_DEVICE_ID)
#define TIMER_LOAD_VALUE (0xFFFFFFFF)
#define APU_FREQ (666666687)
#define PRV_TIMER_NS_1000_CNT (2 * 1000000000000 / APU_FREQ)
enum {
	TIMER_UNINIT = -1,
	TIMER_STOP = 0,
	TIMER_START = 1,
};

static XScuTimer _prv_timer;
static int _prv_timer_sta = TIMER_UNINIT;

int prv_timer_init()
{
	if (_prv_timer_sta == TIMER_UNINIT) {
		// Initialize the Scu Private Timer so that it is ready to use
		XScuTimer_Config *cfg = XScuTimer_LookupConfig(TIMER_DEVICE_ID);
		if (XScuTimer_CfgInitialize(&_prv_timer, cfg,
		                            cfg->BaseAddr) != XST_SUCCESS)
			return -1;

		_prv_timer_sta = TIMER_STOP;
	}

	return 0;
}

int prv_timer_start()
{
	if (_prv_timer_sta == TIMER_STOP) {
		// Load the timer counter register
		XScuTimer_LoadTimer(&_prv_timer, TIMER_LOAD_VALUE);
		XScuTimer_Start(&_prv_timer);

		_prv_timer_sta = TIMER_START;
		return 0;
	}

	return -1;
}

int prv_timer_stop(unsigned int *time_ns)
{
	volatile unsigned int cnt_end;
	
	if (_prv_timer_sta == TIMER_START) {
		cnt_end = XScuTimer_GetCounterValue(&_prv_timer);
		XScuTimer_Stop(&_prv_timer);
		*time_ns = ((TIMER_LOAD_VALUE - cnt_end) * PRV_TIMER_NS_1000_CNT / 1000);

		_prv_timer_sta = TIMER_STOP;
		return 0;
	}

	return -1;
}
