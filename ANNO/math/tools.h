#ifndef _TOOLS_H_
#define _TOOLS_H_

#include "library/common/t_stddef.h"

extern int prv_timer_init();
extern int prv_timer_start();
extern int prv_timer_stop(unsigned int *time_ns);



#endif
